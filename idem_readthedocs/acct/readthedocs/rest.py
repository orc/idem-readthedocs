from typing import Any, Dict

import httpx


def _get_session(hub, token: str, **kwargs):
    # httpx / async
    return httpx.AsyncClient(headers={"Authorization": f"token {token}"})


async def gather(hub) -> Dict[str, Any]:
    sub_profiles = {}
    for profile, ctx in hub.acct.PROFILES.get("readthedocs.rest", {}).items():
        # Add a session to the profile
        sub_profiles[profile] = {"session": _get_session(hub, **ctx)}
        sub_profiles[profile]["project_name"] = ctx["project_name"]
        sub_profiles[profile]["endpoint_url"] = ctx.get(
            "endpoint_url", "https://readthedocs.org/api/v3/projects"
        )
    return sub_profiles
